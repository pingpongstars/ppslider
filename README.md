#Requirements:
link jquery and jquery UI (or better build your own version of jquery ui with widget you need ...)
```html
<link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
  <script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>
```

#Usage:
set html

```html
<div class="container">
    <img src="img1.png" alt="">
    <img src="img2.png" alt="">
    <img src="img3.png" alt="">
</div>
```

set basic css

```css
.ppslider-container {
  position: relative;
  overflow: hidden;
}
.ppslider-container > .ppslider-item {
  position: absolute;
  top: 0;
  left: 0;
  transition: all 0.2s; //for smoother transition
}
```

set a minimum height to your images container
for example: 
```css
.container {
  min-height: 350px;
}
```

call plugin

```javascript
$(".container").ppslider({
  initial: 2,
  debug: false
});
$(".container").on("ppslider:stop", function() {
  console.log("stop");
});
```
