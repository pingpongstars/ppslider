/*!
 * jQuery 
 * Original author: 
 * Licensed 
 */

(function($, window, document, undefined) {
  // Create the defaults once
  var pluginName = "ppslider",
    defaults = {
      initial: 1,
      debug: false,
      effect: 'fadeout'
    };

  // The actual plugin constructor
  function Plugin(element, options) {
    this.element = element;
    this.$el = $(element);
    this.options = $.extend({}, defaults, options);


    this._defaults = defaults;
    this._name = pluginName;

    this.init();
  }

  Plugin.prototype = {
    init: function() {
      this.checkDependencies();

      this.transitioneffect = this.fadeout;
      if (typeof this.options.effect === 'function' ){
        this.transitioneffect = this.options.effect;
      }else{
        switch(this.options.effect.toLowerCase()){
          case "crossfade":
            this.transitioneffect = this.crossfade;
            break;
          case "fadein":
            this.transitioneffect = this.fadein;
            break;
          case "fadeout":
            this.transitioneffect = this.fadeout;
            break;
          default:
            break;
        }
      }

      var self = this;
      self.items = this.$el.children();

      if (self.items.length === 0) {
        console.warn("no children was found in the container");
        return;
      }
      self.current = this.options.initial - 1;
      self.wrapper = this.$el
        .addClass("ppslider-container")
        .wrap('<div class="ppslider-wrapper"></div>')
        .parent();
      self.controller = $('<div class="ppslider-control">').appendTo(
        self.wrapper
      );
      self.controller.slider({
        range: false,
        min: 0,
        max: self.items.length - 1, // number of images
        step: 0.01,
        value: self.current
      });

      self.items.each(function() {
        $(this).addClass("ppslider-item");
      });

      self.controller.on("slidechange slide", function(event, ui) {
        self.transition(ui.value, self.transitioneffect);
        self.$el.trigger("ppslider:slide");
      });

      self.controller.on("slidestop", function(event, ui) {
        self.transition(ui.value, function(current) {
          self.controller.slider("value", current);
          self.$el.trigger("ppslider:stop");
        });
      });
      self.$el.on("ppslider:init", function() {
        self.items.eq(self.current).addClass("current");
        self.transition(self.current, self.transitioneffect);
      });
      self.$el.trigger("ppslider:init");
    },
    checkDependencies: function() {
      if (!$.fn.slider) {
        throw new Error("You need Jquery UI slider");
      }
    },
    transition: function(ui, callback) {
      var current = Math.round(ui);
      var other = current < ui ? current + 1 : current - 1;
      var percentage = Math.abs(ui - current);

      var $current = this.items.eq(current);
      var $other = this.items.eq(other);

      this.items.removeClass("current other");
      $current && $current.addClass("current");
      $other && $other.addClass("other");
      if (this.options.debug) {
        console.log(current, this, $current, $other, percentage);
      }
      callback(current, this, $current, $other, percentage);
    },
    crossfade: function(current, self, $current, $other, percentage) {
      self.items.css({
        opacity: 0,
        "z-index": 1
      });

      $current.css({
        opacity: 1 - percentage,
        "z-index": 10
      });
      $other.css({ opacity: percentage, "z-index": 9 });
    },
    fadein: function(current, self, $current, $other, percentage) {
      self.items.css({
        opacity: 0,
        "z-index": 1
      });

      $current.css({
        opacity: 1,
        "z-index": 10
      });
      $other.css({ opacity: percentage, "z-index": 9 });
    },
    fadeout: function(current, self, $current, $other, percentage) {
      self.items.css({
        opacity: 0,
        "z-index": 1
      });

      $current.css({
        opacity: 1,
        "z-index": 10
      });
      $current.css({
        opacity: 1.2 - percentage,
        "z-index": 10
      });
      $other.css({ opacity: 1, "z-index": 9 });
    }
  };

  // A really lightweight plugin wrapper around the constructor,
  // preventing against multiple instantiations
  $.fn[pluginName] = function(options) {
    return this.each(function() {
      if (!$.data(this, "plugin_" + pluginName)) {
        $.data(this, "plugin_" + pluginName, new Plugin(this, options));
      }
    });
  };
})(jQuery, window, document);
